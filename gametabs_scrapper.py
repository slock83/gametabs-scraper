#%%
from bs4 import BeautifulSoup
import requests
import json
import logging

log = logging.getLogger(__name__)
log.setLevel(logging.INFO)

#%%


#%%
class GamesFinder:
    def __init__(self, base_url="https://www.gametabs.net/"):
        self.platforms = {}
        self.games = {}
        self.series = {}
        self.tabs = {}
        self.base_url = base_url
    
    def _get_pages_urls(self, section="games"):
        url = self.base_url + section
        response = requests.get(url)
        soup = BeautifulSoup(response.content, "lxml")
        url_list = []
        try:
            page_count = int(soup.find("li", class_="pager-last").find("a")['href'].split('=')[1])
            url_list = [url + "?page={}".format(str(i)) for i in range(page_count+1)]
        except AttributeError:
            url_list = [self.base_url + section]
        return url_list

    def _get_grid_rows(self, section="games"):
        urls = self._get_pages_urls(section)
        for url in urls:
            response = requests.get(url)
            soup = BeautifulSoup(response.content, "lxml")
            rows = soup.find('tbody').find_all('tr')
            for row in rows:
                yield row

    def get_all_games(self):
        for row in self._get_grid_rows():
            elem = {}
            elem_basic = row.find("td", class_="views-field-name").find('a')
            elem['url'] = elem_basic['href'].lstrip('/')
            elem['name'] = elem_basic.string
            elem_plat = row.find("td", class_="views-field-name-1").find('a')
            elem['platform'] = elem_plat.string
            self.platforms[elem_plat.string] = elem_plat['href']
            elem_count = row.find("td", class_="views-field-node-count").string
            elem['tabs'] = int(elem_count.strip())
            self.games[elem['url']] = elem
            self.enrich_game(elem['url'])
    
    def get_all_series(self):
        for row in self._get_grid_rows(section="series"):
            serie = {}
            serie_basic = row.find("td", class_="views-field-name").find('a')
            serie['url'] = serie_basic['href'].lstrip('/')
            serie['name'] = serie_basic.string
            self.series[serie['url']] = serie

    def enrich_series(self):
        count = 0
        for serie in self.series.values():
            count += 1
            if count > 3:
                break
            log.info('Enriching {}'.format(serie['url']))
            urls = []
            try:
                urls = self._get_pages_urls(section=serie['url'])
            except AttributeError:
                urls = [self.base_url + serie['url']]
            series_games = set()
            for url in urls:
                response = requests.get(url)
                soup = BeautifulSoup(response.content, "lxml")
                try:
                    rows = soup.find('tbody').find_all('tr')
                    for row in rows:
                        serie_basic = row.find("td", class_="views-field-name").find('a')
                        series_games.add(serie_basic['href'].lstrip('/'))
                except AttributeError:
                    log.warning('Apparently, {} has no tabs'.format(serie['url']))
                
            self.series[serie['url']]['games'] = list(series_games)
            for game_id in series_games:
                try:
                    self.games[game_id]['serie'] = serie['url']
                except KeyError:
                    log.warning('Game {} not scrapped'.format(game_id))

    def enrich_game(self, game_url):
        tabs = []
        for row in self._get_grid_rows(game_url):
            tab = {}
            tab_basic = row.find("td", class_="views-field-title").find('a')
            tab['url'] = tab_basic['href'].lstrip('/')
            tab['name'] = tab_basic.string
            tab['instruments'] = [inst.string for inst in row.find("td", class_="views-field-title").findAll('span')]
            tabber = {}
            tabber_basic = row.find("td", class_="views-field-name-1").find('a')
            tabber['url'] = tabber_basic['href'].lstrip('/')
            tabber['name'] = tabber_basic.string
            tab['tabber'] = tabber
            tabs.append(tab)
            self.tabs[tab['url']] = tab
        self.games[game_url]['tabs'] = tabs

    def dump_games(self):
        with open('games.json', 'w') as out_games:
            json.dump(self.games, out_games)
    
    def dump_platforms(self):
        with open('platforms.json', 'w') as out_platforms:
            json.dump(self.platforms, out_platforms)

    def dump_series(self):
        with open('series.json', 'w') as out_series:
            json.dump(self.series, out_series)

gf = GamesFinder()
gf.get_all_games()
# gf.get_all_series()
# gf.enrich_series()
# gf.dump_series()
